# post-jam TODO

* [x] space out the top-right module array
* [x] slow down turn rate
* [x] move rovers backwards
* [x] start the game with the laser aimed backwards
* [x] draw laser under "high" layer
* [x] out-of-order tutorial items
* [x] immobilize rovers that get stuck in doors
* [x] sound effects
 * [x] laser hum (13599? 51265)
 * [x] door opening (57560)
 * [x] dock/undock (2110)
 * [x] terminal activation bleep
* [X] animate opening of doors
* [X] scroll bug/speed up
* [X] number unselected deployed rovers
* [ ] make the emitter actually look like a satellite dish
* [ ] give HUD messages transparent bg
* [X] build appimage files
* [X] automate uploads to itch.io
* [ ] a few more screenshots

# during jam

* [X] rover movement
* [X] selecting between rovers
* [X] main probe movement
* [X] initial area map
* [-] rocks that can be pushed
* [X] laser aiming
* [X] laser reflection
* [-] russian base map
* [X] doors
* [X] terminals
* [-] reactor with activation
* [X] tutorial
* [-] other map elements that can be activated by laser?
* [X] writing terminal text
* [X] level design
* [X] HUD
* [X] in-game message system
* [-] writing for russian terminals
* [X] writing for american terminals
* [X] end game
* [X] hold down shift to decrease speed
* [X] show help text on pause screen
* [X] beam splitter
